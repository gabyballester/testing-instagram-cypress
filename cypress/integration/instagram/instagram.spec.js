// Contexto engloba todo el test que dentro tiene dos pruebas
context('Navegación por google', () => { //El nombre del objetivo lo definimos aquí

    // Con 'it' definimos una prueba y damos nombre a la misma
    it('Visitar la página de Google', () => {
        //Con la palabra clave cy y el metodo visit y la página a visitar
        cy.visit('https://www.google.com/');
    });

    it('Búsqueda de Instagram', () => {
        //selector del input de google
        cy.get('.gLFyf').type('Instagram'); //Navegador introduce palabra en el input
    });

    it('Hacemos click en botón de búsqueda de google', () => {
        //accionamos el botón de búsqueda de Google con la cadena previa
        cy.get('.aajZCb > .tfB0Bf > center > .gNO89b').click();
    });

    it('Hacer click primer enlace y entrar en la web', () => {
        // Hacemos click en el primer elemento de la búsqueda
        cy.get('[data-hveid="CBEQAA"] > :nth-child(2) > .rc > .r > a > .LC20lb').click();
    });

});
